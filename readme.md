# Diablo 2 name genarator

A name generator that uses Diablo 2's prefixes and suffixes for rare items.

## Usage

```
Usage: d2-names [OPTION...]

Options:
    -?                  Show this help doc then exit.
    -t                  Base word type. Choices are: Armor, Helm, Bow, Axe,
                        Mace, Amulet, Belt, Shield, Scepter, Sword, Spear,
                        Ring, Glove, Boot, Stave.
```
