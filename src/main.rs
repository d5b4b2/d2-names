// Copyright 2018 Gerrit Viljoen

// This file is part of d2-names.
//
// d2-names is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// d2-names is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with d2-names.  If not, see <http://www.gnu.org/licenses/>.

mod words;

extern crate getopts;
use getopts::{HasArg, Occur, Options};

extern crate rand;
use rand::random;

use std::env::args;
use std::fmt::{Debug, Display};

const OPTS_SYNTAX: &str = "d2-names [OPTION...]";
const HELP_OPT: &str = "?";
const TYPE_OPT: &str = "t";
const HELP_DESC: &str = "Show this help doc then exit.";
const TYPE_DESC: &str = "Base word type. Choices are: Armor, Helm, Bow, Axe, Mace, Amulet, Belt, Shield, Scepter, Sword, Spear, Ring, Glove, Boot, Stave.";

fn main() -> Result<(), MainError> {
    let opts = options();
    let popts = opts.parse(args().skip(1))?;

    if popts.opt_present(HELP_OPT) {
        print!("Usage: {}", opts.usage(OPTS_SYNTAX));
        return Ok(())
    }

    let prefix = choose(words::PREFIX);
    let word = choose(match popts.opt_str(TYPE_OPT) {
        Some(ref name) => match for_name(name) {
            Some(x) => x,
            None => return err(format!("Invalid option -{}", TYPE_OPT))
        },
        None => random_set()
    });
    println!("{} {}", prefix, word);
    Ok(())
}

fn choose<T>(arr: &[T]) -> &T {
    &arr[random::<usize>() % arr.len()]
}

fn random_set() -> &'static [&'static str] {
    let set = &[
        words::ARMOR, words::HELM, words::BOW, words::AXE, words::MACE,
        words::AMULET, words::BELT, words::SHIELD, words::SCEPTER,
        words::SWORD, words::SPEAR, words::RING, words::GLOVE, words::BOOT,
        words::STAVE
    ];
    choose::<&[&str]>(set)
}

fn for_name(name: &str) -> Option<&'static [&'static str]> {
    match name {
        "Armor" => Some(words::ARMOR),
        "Helm" => Some(words::HELM),
        "Bow" => Some(words::BOW),
        "Axe" => Some(words::AXE),
        "Mace" => Some(words::MACE),
        "Amulet" => Some(words::AMULET),
        "Belt" => Some(words::BELT),
        "Shield" => Some(words::SHIELD),
        "Scepter" => Some(words::SCEPTER),
        "Sword" => Some(words::SWORD),
        "Spear" => Some(words::SPEAR),
        "Ring" => Some(words::RING),
        "Glove" => Some(words::GLOVE),
        "Boot" => Some(words::BOOT),
        "Stave" => Some(words::STAVE),
        _ => None
    }
}

fn options() -> Options {
    let mut res = Options::new();
    res.opt(HELP_OPT, "", HELP_DESC, "", HasArg::No, Occur::Optional);
    res.opt(TYPE_OPT, "", TYPE_DESC, "", HasArg::Yes, Occur::Optional);
    res
}

enum MainError {
    Custom(String),
    Opts(::getopts::Fail)
}

impl Debug for MainError {

    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        use MainError::*;
        match self {
            Custom(x) => write!(f, "{}", x),
            Opts(x) => Debug::fmt(x, f)
        }
    }
}

impl From<::getopts::Fail> for MainError {

    fn from(x: ::getopts::Fail) -> Self {
        MainError::Opts(x)
    }
}

fn err<T>(msg: T) -> Result<(), MainError>
where T: Display {
    Err(MainError::Custom(msg.to_string()))
}
