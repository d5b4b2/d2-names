// Copyright 2018 Gerrit Viljoen

// This file is part of d2-names.
//
// d2-names is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// d2-names is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with d2-names.  If not, see <http://www.gnu.org/licenses/>.

pub const PREFIX: &[&str] = &[
    "Armageddon",
    "Beast",
    "Bitter",
    "Blood",
    "Bone",
    "Bramble",
    "Brimstone",
    "Carrion",
    "Chaos",
    "Corpse",
    "Corruption",
    "Cruel",
    "Death",
    "Demon",
    "Dire",
    "Dread",
    "Doom",
    "Eagle",
    "Empyrian",
    "Entropy",
    "Fiend",
    "Gale",
    "Ghoul",
    "Glyph",
    "Grim",
    "Hailstone",
    "Havoc",
    "Imp",
    "Loath",
    "Order",
    "Pain",
    "Plague",
    "Raven",
    "Rift",
    "Rune",
    "Shadow",
    "Skull",
    "Soul",
    "Spirit",
    "Stone",
    "Storm",
    "Viper",
    "Wraith"
];
pub const ARMOR: &[&str] = &[
    "Carapace",
    "Cloak",
    "Coat",
    "Hide",
    "Jack",
    "Mantle",
    "Pelt",
    "Shroud",
    "Suit",
    "Wrap"
];
pub const HELM: &[&str] = &[
    "Brow",
    "Casque",
    "Circlet",
    "Cowl",
    "Crest",
    "Hood",
    "Horn",
    "Mask",
    "Veil",
    "Visage",
    "Visor"
];
pub const BOW: &[&str] = &[
    "Bolt",
    "Branch",
    "Fletch",
    "Flight",
    "Horn",
    "Nock",
    "Quarrel",
    "Quill",
    "Song",
    "Stinger",
    "Thirst"
];
pub const AXE: &[&str] = &[
    "Beak",
    "Bite",
    "Cleaver",
    "Edge",
    "Gnash",
    "Mangler",
    "Reaver",
    "Rend",
    "Scythe",
    "Sever",
    "Slayer",
    "Song",
    "Spawn",
    "Splitter",
    "Sunder",
    "Thirst"
];
pub const MACE: &[&str] = &[
    "Bane",
    "Blow",
    "Brand",
    "Break",
    "Crack",
    "Crusher",
    "Grinder",
    "Knell",
    "Mallet",
    "Ram",
    "Smasher",
    "Star"
];
pub const AMULET: &[&str] = &[
    "Beads",
    "Collar",
    "Gorget",
    "Heart",
    "Necklace",
    "Noose",
    "Scarab",
    "Talisman",
    "Torc"
];
pub const BELT: &[&str] = &[
    "Buckle",
    "Chain",
    "Clasp",
    "Cord",
    "Fringe",
    "Harness",
    "Lash",
    "Lock",
    "Strap",
    "Winding"
];
pub const SHIELD: &[&str] = &[
    "Aegis",
    "Badge",
    "Emblem",
    "Guard",
    "Mark",
    "Rock",
    "Shield",
    "Tower",
    "Ward",
    "Wing"
];
pub const SCEPTER: &[&str] = &[
    "Blow",
    "Breaker",
    "Call",
    "Chant",
    "Crack",
    "Crusher",
    "Cry",
    "Gnarl",
    "Grinder",
    "Knell",
    "Ram",
    "Smasher",
    "Song",
    "Spell",
    "Star",
    "Weaver"
];
pub const SWORD: &[&str] = &[
    "Barb",
    "Bite",
    "Cleaver",
    "Edge",
    "Fang",
    "Gutter",
    "Impaler",
    "Needle",
    "Razor",
    "Saw",
    "Scalpel",
    "Scratch",
    "Scythe",
    "Sever",
    "Skewer",
    "Spiker",
    "Song",
    "Stinger",
    "Thirst",
    "Wand"
];
pub const SPEAR: &[&str] = &[
    "Barb",
    "Branch",
    "Dart",
    "Fang",
    "Goad",
    "Gutter",
    "Impaler",
    "Lance",
    "Nails",
    "Needle",
    "Prod",
    "Scourge",
    "Scratch",
    "Skewer",
    "Spike",
    "Stinger",
    "Wand",
    "Wrack"
];
pub const RING: &[&str] = &[
    "Band",
    "Circle",
    "Coil",
    "Eye",
    "Finger",
    "Grasp",
    "Grip",
    "Gyre",
    "Hold",
    "Knuckle",
    "Loop",
    "Nails",
    "Spiral",
    "Touch",
    "Turn",
    "Whorl"
];
pub const GLOVE: &[&str] = &[
    "Claw",
    "Clutches",
    "Finger",
    "Fist",
    "Grasp",
    "Grip",
    "Hand",
    "Hold",
    "Knuckle",
    "Touch"
];
pub const BOOT: &[&str] = &[
    "Slippers",
    "Spur"
];
pub const STAVE: &[&str] = &[
    "Branch",
    "Call",
    "Chant",
    "Cry",
    "Goad",
    "Gnarl",
    "Spell",
    "Spire",
    "Song",
    "Weaver"
];
